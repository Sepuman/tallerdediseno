import psycopg2
import datetime
import time

basededatos = 'dbname=simon user=simon'
#Agrega una tupla a la base de datos
#nombre: es el nombre del archivo
#dia, mes, ano (ano), hora componene el momento de grabacion
#radio: es el nombre de la radio guardada
def agregar_db(radio, momento, nombre):
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()
	cur.execute('INSERT INTO archivos (radio, momento, nombre) VALUES (%s, %s, %s)',(radio, momento, nombre))
	conn.commit()
	cur.close()
	conn.close()

def agregar_radio(radio, link):
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()

	#Buscar si la radio ya esta
	cur.execute('SELECT*FROM radios WHERE radio = %s', (radio,))
	n = cur.rowcount
	if n != 0:
		print 'El link de la radio ya esta en la base de datos'
	else:
		cur.execute('INSERT INTO radios (radio, link) VALUES (%s, %s)',(radio, link))
		conn.commit()
		cur.close()
		conn.close()

def eliminar_radio(radio):
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()

	#Buscar si la radio ya esta
	cur.execute('SELECT*FROM radios WHERE radio = %s', (radio,))
	n = cur.rowcount
	
	if n == 0:
		print 'La radio no esta en la base de datos'
	else:
		cur.execute('DELETE FROM radios WHERE radio = %s',(radio,))
		conn.commit()
		cur.close()
		conn.close()

def modificar_link(radio, link):
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()

	#Buscar si la radio ya esta
	cur.execute('SELECT*FROM radios WHERE radio = %s', (radio,))
	n = cur.rowcount

	if n == 0:
		print 'La radio no esta en la base de datos'
	else:
		cur.execute('UPDATE radios SET link = %s WHERE radio = %s',(link,radio))

def mostrar_radios():
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()
	cur.execute('SELECT radio FROM radios')
	n = cur.rowcount
	if n == 0:
		print 'La base de datos esta vacia'
	else:
		radios = cur.fetchmany(n)
		for radio in radios:
			print radio

def radios_disponibles():
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()
	cur.execute('SELECT radio FROM radios')
	n = cur.rowcount
	if n == 0:
		return []
	else:
		radios = cur.fetchmany(n)
		return radios

def nombre_archivo(radio,fecha,hora,lgrab):
	k1 = fecha.split("-")
	k2 = hora.split(":")
	c1 = 1900<=int(k1[0])<=2200
	c2 = 1<=int(k1[1])<=12
	c3 = 1<=int(k1[2])<=31
	c4 = 0<=int(k2[0])<=23
	c5 = 0<=int(k2[1])<=59
	c6 = 0<=int(k2[2])<=59
	if not(c1) or not(c2) or not(c3) or not(c4) or not(c5) or not(c6):
		print 'Fecha invalida'
		return []	
	busqueda = datetime.datetime(int(k1[0]),int(k1[1]),int(k1[2]),int(k2[0]),int(k2[1]),int(k2[2]))
	sbusqueda = time.mktime(busqueda.timetuple())
	sbusme = sbusqueda - lgrab/2
	sbusma = sbusqueda + lgrab/2
	sbusmenor = datetime.datetime.fromtimestamp(sbusme).strftime('%Y-%m-%d %H:%M:%S')
	sbusmayor = datetime.datetime.fromtimestamp(sbusma).strftime('%Y-%m-%d %H:%M:%S')
	conn = psycopg2.connect(basededatos)
	cur = conn.cursor()
	cur.execute('SELECT nombre FROM archivos WHERE radio = %s AND momento < %s AND momento > %s', (radio,sbusmayor,sbusmenor))
	n = cur.rowcount
	if n == 0:
		print 'No hay archivos'
		return []
	else:
		nombre = cur.fetchmany(n)
		return nombre[0][0]














