import urllib2
import time
import threading
import datetime
import psycopg2
import os 
from base_de_datos import agregar_db
from calendar import timegm

#nombre: string con el nombre de la radio
#momento: string con el momento en que se graba
#tiempo: tiempo que se graba en segundos
def grabar_radio_real(nombre,link,tiempo):
	camino = os.path.abspath('Musica')
	#Recibe el string nombre de la radio a grabar y el link
	print threading.currentThread().getName(), 'inicia grabado'
	ts = time.time()
	momento = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
	nmomento = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
	segundos = tiempo
	f = open(camino+ '/' + nombre + nmomento + ".mp3", "w+b") #w+b significa escribir en formato binario
	stream = urllib2.urlopen(link) #link de la radio a grabar
	c = 0
	print str(stream.info())
	while c < segundos:
		f.write(stream.read(64000/8))
		c = c + 1
	f.close()
	filename =camino+ '/' + nombre + nmomento + '.mp3'
	agregar_db(nombre, momento, filename)
	print threading.currentThread().getName(), 'termina grabado'
	
	
#Agrega una tupla a la base de datos
#nombre: es el nombre del archivo
#dia, mes, ano (ano), hora componene el momento de grabacion
#radio: es el nombre de la radio guardada
def eliminar_antiguos(fecha):
	conn = psycopg2.connect('dbname=simon user=simon')
	cur = conn.cursor()
	k1 = fecha.split("-")
	a = k1[2]
	k2 = a.split(' ')
	b = k2[1]
	k3 = b.split(':')
	AAAA = int(k1[0])
	MM = int(k1[1])
	DD = int(k2[0])
	HH = int(k3[0])
	mm = int(k3[1])
	ss = int(k3[2])
	cur.execute('SELECT nombre FROM archivos WHERE momento < %s', (datetime.datetime(AAAA,MM,DD,HH,mm,ss),))
	for nombres in cur:
		os.remove(nombres[0])
	cur.execute('DELETE FROM archivos WHERE momento < %s',(datetime.datetime(AAAA,MM,DD,HH,mm,ss),))
	conn.commit()
	cur.close()
	conn.close()
