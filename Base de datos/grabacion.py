import psycopg2
import threading
import time
import datetime
import os
from funciones_streaming import grabar_radio_real, eliminar_antiguos

#Inicializaciones
conn = psycopg2.connect('dbname=simon user=simon')
cur = conn.cursor()
fecha1 = time.time() #ultimo momento de grabacion
threads = list() #Lista de threads
lradio = list()
lhoras = list()
radios = list()

#Tiempo de duracion de cada archivo

lgrab = 10*60

#Tiempo de antiguedad
alimite = 24*4*7

while True:
	fecha2 = time.time() #actual ciclo
	d = (fecha2-fecha1) #cuanto ha pasado desde que comenzo la grabacion anterior
	if d>lgrab and len(radios) > 0:
		for radio in radios:
			t = threading.Thread(target=grabar_radio_real, name=radio[0], args=(radio[0],radio[1],lgrab))
			threads.append(t)
			t.start()
			lradio.append(radio[0])
			lhoras.append(radio[1])
		fecha1 = fecha2
	cur.execute('SELECT COUNT(*) FROM archivos')
	if cur.fetchone() > alimite:
		fecha = fecha2-lgrab*alimite
		fecha = datetime.datetime.fromtimestamp(fecha).strftime('%Y-%m-%d %H:%M:%S')
		eliminar_antiguos(fecha)
	cur.execute('SELECT*FROM radios')
	n = cur.rowcount
	radios = cur.fetchmany(n)


