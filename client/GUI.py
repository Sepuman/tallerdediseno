import Tkinter as tk
import time
import vlc
import sys

class GUI(tk.Frame):
    def __init__(self, parent):
	self.TEXT=''
	self.s = 0
	self.s1 = 0
	self.s2 = 0
	self.i=0
	self.p=0
	self.s=0
	self.parent = parent
	tk.Frame.__init__(self, parent)
        parent.title("Radio IP")
	parent.geometry('250x320')
	parent.config(bg='white')

        label = tk.Label(self, text="Instrucciones")
        label.pack()

	radios_button = tk.Button(self, text="Lista de Radios", command=self.radios, bg='#33FFFF')
        radios_button.pack()
        
	cambiar_button = tk.Button(self, text="Cambiar Radio", command=self.cambiar, bg='#33FFFF')
        cambiar_button.pack()

        rewind_button = tk.Button(self, text="Retroceder", command=self.rewind, bg='#33FFFF')
        rewind_button.pack()

        forward_button = tk.Button(self, text="Adelantar", command=self.forward, bg='#33FFFF')
        forward_button.pack()

	play_button = tk.Button(self, text="Reproducir", command=self.play, bg='#33FFFF')
        play_button.pack()

	pause_button = tk.Button(self, text="Pausar", command=self.pause, bg='#33FFFF')
        pause_button.pack()

	vivo_button = tk.Button(self, text="En Vivo", command=self.vivo, bg='#33FFFF')
        vivo_button.pack()

        close_button = tk.Button(self, text="Cerrar", command=self.salir, bg='#33FFFF')
        close_button.pack()

    def cambiar(self):
	window = tk.Toplevel(self)
	fr1 = tk.Frame(window,bg='#a1dbcd')
        fr2 = tk.Frame(window,bg='#a1dbcd')
        fr3 = tk.Frame(window,bg='#a1dbcd')
        fr4 = tk.Frame(window,bg='#a1dbcd')
        label1 = tk.Label(fr1, text="Nombre de Radio: ")
        label2 = tk.Label(fr2, text="Fecha (AAAA-MM-DD): ")
        label3 = tk.Label(fr3, text="Hora (HH:MM:SS): ")
	self.ent1=tk.Entry(fr1)
	self.ent2=tk.Entry(fr2)
	self.ent3=tk.Entry(fr3)
        b = tk.Button(fr4, text="Listo", command=self.savecambiar, bg='#33FFFF')
	fr1.pack()
        label1.pack(side='left')
	self.ent1.pack(side='left')
	fr2.pack()
        label2.pack(side='left')
	self.ent2.pack(side='right')
	fr3.pack()
        label3.pack(side='left')
	self.ent3.pack(side='left')
	fr4.pack()
	b.pack()
	
    def savecambiar(self):
	radio=self.ent1.get()
	fecha=self.ent2.get()
	hora=self.ent3.get()
	TEXT='cambiar '+radio+' '+fecha+' '+hora
	self.s1.sendall(TEXT.encode())
	time.sleep(1)
	self.i = vlc.Instance('--verbose 2'.split())
	self.p = self.i.media_player_new()
	self.p.set_mrl('rtsp://213.136.90.188:'+str(self.s.getsockname()[1])+"/")
	self.p.play()

    def rewind(self):
	window = tk.Toplevel(self)
	fr1 = tk.Frame(window,bg='#a1dbcd')
        fr4 = tk.Frame(window,bg='#a1dbcd')
        label1 = tk.Label(fr1, text="Tiempo de retroceso (s): ")
	self.ent1=tk.Entry(fr1)
        b = tk.Button(fr4, text="Listo", 
                      command=self.saverewind, bg='#33FFFF')
	fr1.pack()
        label1.pack(side='left')
	self.ent1.pack(side='left')
	fr4.pack()
	b.pack()

    def saverewind(self):
	tiempo=self.ent1.get()
	TEXT='rewind '+ tiempo
	self.s1.sendall(TEXT.encode())
	
    def forward(self):
	window = tk.Toplevel(self)
	fr1 = tk.Frame(window,bg='#a1dbcd')
        fr4 = tk.Frame(window,bg='#a1dbcd')
        label1 = tk.Label(fr1, text="Tiempo de avance (s): ")
	self.ent1=tk.Entry(fr1)
        b = tk.Button(fr4, text="Listo", 
                      command=self.saveforward, bg='#33FFFF')
	fr1.pack()
        label1.pack(side='left')
	self.ent1.pack(side='left')
	fr4.pack()
	b.pack()

    def saveforward(self):
	tiempo=self.ent1.get()
	TEXT='forward '+ tiempo
	self.s1.sendall(TEXT.encode())
	
    def radios(self):
        self.s1.sendall('radios'.encode())
	radios = self.s1.recv(1024).decode()
	listaradios=radios.split()
	window = tk.Toplevel(self)
	window.geometry('250x100')
	for i in range(len(listaradios)):
		fr1 = tk.Frame(window,bg='#a1dbcd')
        	label1 = tk.Label(fr1, text=listaradios[i])
		fr1.pack()
        	label1.pack(side='left')

    def play(self):
	self.s1.sendall('play'.encode())
	time.sleep(1)
	self.i = vlc.Instance('--verbose 2'.split())
	self.p = self.i.media_player_new()
	self.p.set_mrl('rtsp://213.136.90.188:'+str(self.s.getsockname()[1])+"/")
	self.p.play()

    def pause(self):
	self.s1.sendall('pause'.encode())
	time.sleep(1)
	self.i = vlc.Instance('--verbose 2'.split())
	self.p = self.i.media_player_new()
	self.p.set_mrl('rtsp://213.136.90.188:'+str(self.s.getsockname()[1])+"/")
	self.p.pause()

    def vivo(self):
	window = tk.Toplevel(self)
	fr1 = tk.Frame(window,bg='#a1dbcd')
        fr4 = tk.Frame(window,bg='#a1dbcd')
        label1 = tk.Label(fr1, text="Nombre de Radio: ")
	self.ent1=tk.Entry(fr1)
        b = tk.Button(fr4, text="Listo", 
                      command=self.savevivo, bg='#33FFFF')
	fr1.pack()
        label1.pack(side='left')
	self.ent1.pack(side='left')
	fr4.pack()
	b.pack()

    def savevivo(self):
	radio=self.ent1.get()
	TEXT='vivo '+ radio
	self.s1.sendall(TEXT.encode())
	time.sleep(1)
	self.i = vlc.Instance('--verbose 2'.split())
	self.p = self.i.media_player_new()
	self.p.set_mrl('rtsp://213.136.90.188:'+str(self.s.getsockname()[1])+"/")
	self.p.play()

    def salir(self):
	self.s1.sendall('exit'.encode())
        self.s1.close()
        self.s.close()
        self.s2.close()
        self.parent.destroy()
        sys.exit(1)

if __name__ == "__main__":
	root = tk.Tk()
	GUI(root).pack(fill="both", expand=True)
	root.mainloop()
