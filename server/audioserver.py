# Echo server program
import socket
import sys
import threading
from act_pointer import *
from enviar_musica import enviar_musica
from navegacion import navegacion
import random
import string
from Crypto.Cipher import AES
import vlc


HOST = 'LOCALHOST'               # Symbolic name meaning all available interfaces
PORT = 50024              # Arbitrary non-privileged port
threads = list()
threadnav = list()

s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC,
							  socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
	af, socktype, proto, canonname, sa = res
	try:
		s = socket.socket(af, socktype, proto)
	except socket.error as msg:
		s = None
		continue
	try:
		s.bind(sa)
		s.listen(1)
	except socket.error as msg:
		s.close()
		s = None
		continue
	break
if s is None:
	print ('could not open socket')
	sys.exit(1)

PORT = 40025
s1 = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC,
							  socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
	af, socktype, proto, canonname, sa = res
	try:
		s1 = socket.socket(af, socktype, proto)
	except socket.error as msg:
		s1 = None
		continue
	try:
		s1.bind(sa)
		s1.listen(1)
	except socket.error as msg:
		s1.close()
		s1 = None
		continue
	break
if s1 is None:
	print ('could not open socket')
	sys.exit(1)

inst = vlc.Instance()
while True:
	try:
		conn, addr = s.accept()
	except KeyboardInterrupt:
		s.close()
		s1.close()
		sys.exit(1)
	code = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16))
	print(code)
	act = act_pointer()
	t = threading.Thread(target=enviar_musica, args=(conn, addr,act,code,inst))
	threads.append(t)
	t.start()
	conn1, addr1 = s1.accept()
	nav = threading.Thread(target=navegacion, args=(conn1, addr1,act))
	threadnav.append(nav)
	nav.start()
	
s.close()
s1.close()
	

