import sys
from act_pointer import *
sys.path.insert(0, '/home/simon/tallerdediseno/Base de datos')
from base_de_datos import *

def navegacion(conn1, addr1, act):
	print 'conectado para navegar a: ', addr1
	while True:
		data = conn1.recv(1024)
		pp = data.decode().split()
		if pp[0] == 'rewind':
			duracion = act.p.get_length()
			msPor = 1.0 / duracion
			restar = float(pp[1]) * 1000 * msPor
			ir = act.p.get_position() - restar
			if ir >= 0:
				act.p.set_position(ir)
		elif pp[0] == 'forward':
			duracion = act.p.get_length()
			msPor = 1.0 / duracion
			sumar = float(pp[1]) * 1000 * msPor
			ir = act.p.get_position() + sumar
			if ir <= 1:
				act.p.set_position(ir)
		elif pp[0] == 'up':
			print ('volumen parriba')
		elif pp[0] == 'down':
			print ('volumen pabajo')
		elif pp[0] == 'pause':
			act.p.pause()
		elif pp[0] == 'play':
			act.p.play()
		elif pp[0] == 'radios':
			listRadios = ""
			listado = radios_disponibles()
			listado.append("fin")
			for radio in listado[0:len(listado)-1]:
				listRadios = listRadios + str(radio[0]) + " "
			conn1.sendall(listRadios.encode())
		elif pp[0] == 'por':
			if len(pp) == 2:
				act.p.set_position(float(pp[1]))
		elif pp[0] == 'cambiar':
			print data.decode()
			radio = data.decode().split()[1]
			fecha = data.decode().split()[2]
			hora = data.decode().split()[3]
			nombre = nombre_archivo(radio,fecha,hora,15)
			act.cambiar('Base de datos/' + nombre)
		else:
			print data.decode()

	conn1.close()
